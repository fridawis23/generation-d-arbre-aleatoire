import java.util.*;

public class Aldous_Broder {
    Graph graph;
    int initialeVertex;
    ArrayList<Edge> tree = new ArrayList<>() ;
    Arc[] edges  ;
    Set<Integer> vertices = new HashSet<>();

    public Aldous_Broder(Graph graph){
        this.graph=graph;
        initialeVertex= graph.getVertices().get(new Random().nextInt(graph.order));
        vertices.add(initialeVertex);
        edges = new Arc[graph.order];

    }
    private ArrayList<Edge> findAleatoirePaths() {
        int sommetActuel = initialeVertex;
        while(!vertices.containsAll(graph.getVertices())){
            updateAleatoirePath(sommetActuel);
            sommetActuel = (int) vertices.toArray()[new Random().nextInt(vertices.size())];
        }
        for (int i=0;i<edges.length;i++) {
            if(!tree.contains(edges[i].support)){
                tree.add(edges[i].support);
            }
        }
        return tree;
    }

    private void updateAleatoirePath(int u) {
        List<Arc> adjacency = graph.getOutAdjacency().get(u);
        Arc arc = adjacency.get(new Random().nextInt(adjacency.size()));
        while(edges[arc.getDest()]==null){
            edges[arc.getDest()] = arc;
            vertices.add(arc.getSource());
            vertices.add(arc.getDest());
            adjacency = graph.getOutAdjacency().get(arc.getDest());
            arc = adjacency.get(new Random().nextInt(adjacency.size()));
        }

    }


    public static ArrayList<Edge> generateTree(Graph graph) {
        Aldous_Broder algo = new Aldous_Broder(graph);
        return algo.findAleatoirePaths();
    }


}
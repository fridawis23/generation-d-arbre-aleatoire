

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;
import java.util.Stack;

public class RandomParcours {
    Graph graph;
    Stack<Arc> frontier;
    BitSet reached;
    ArrayList<Arc> predecessor;

    private void etendsFrontiere(int sommet) {
        for (Arc a : graph.outNeighbours(sommet))
            frontier.add(a);
    }


    private void explore(Arc a) {
        if (reached.get(a.getDest())) return;
        reached.set(a.getDest());
        etendsFrontiere(a.getDest());
        predecessor.add(a);
    }

    private void parcours(int source) {
        reached.set(source);
        etendsFrontiere(source);
        while (!frontier.isEmpty())
            explore(frontier.remove(new Random().nextInt(frontier.size())));

    }

    private RandomParcours(Graph graph) {
        this.graph = graph;
        this.frontier = new Stack<>();
        this.reached = new BitSet(graph.order);
        this.predecessor = new ArrayList<>();
    }


    public static ArrayList<Arc> generateTree(Graph graph, int source) {
        RandomParcours p = new RandomParcours(graph);
        p.parcours(source);
        return p.predecessor;
    }
}

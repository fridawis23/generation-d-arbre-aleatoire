

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Random;

public class Prim {

    Graph graph;
    ArrayList<Edge> tree;
    PriorityQueue F;
    double [] cout;
    static int infini=10;
    ArrayList<Edge> edges;


    public Prim(Graph graph){
        this.graph=graph;
        this.tree=new ArrayList<>();
        this.edges=graph.getAllEdge();
        F=new PriorityQueue(this.comparator());
        cout=new double[graph.order+1];
    }
    public void uniformWeight(){
        Random random=new Random();
        for(Edge edge:edges){
            edge.setWeight(random.nextDouble());
        }
    }

    private Comparator comparator(){
        return new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                double dif= cout[(int) o2]-cout[(int) o1];
                if(dif==0){
                    return 0;
                }else{
                    if (dif < 0){
                        return 1;
                    }else {
                        return -1;
                    }
                }
            }
        };
    }
    private void prim(int root){
        for(int i=0;i< graph.order ;i++){
            cout[i]=infini;
            tree.add(i,null);
        }
        cout[root]=0;
        F.addAll(graph.getVertices());
        while (!F.isEmpty()){
            int t= (int) F.peek();
            for(Edge u:edges){
                if(u.source == t){
                   if(cout[u.dest]>= u.getWeight()) {
                       tree.remove(u.dest);
                       tree.add(u.dest, u);
                       cout[u.dest] = u.getWeight();
                   }
                }
            }
            F.remove();
        }
        tree.remove(0);
    }
    public static ArrayList<Edge> generateTree(Graph graph, int root){
        Prim algo=new Prim(graph);
        algo.uniformWeight();
        algo.prim(root);
        return algo.tree;
    }
}

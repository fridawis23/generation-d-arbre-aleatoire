# Génération-d-arbre-aléatoire 

# J’ai implémenté trois Algorithmes 

Le parcours aléatoire 
L’algorithme de Prim 
L’Algorithme d’Aldous Broder 
 
L'Algorithme de Prim utilisé a été récupéré sur Wikipédia 
:https://fr.wikipedia.org/wiki/Algorithme_de_Prim#Pseudo-code 

Pour utilise ce programme il suffit de prendre soin d’indiquer l’algorithme de 
son choix dans la méthode ” geneTree” de la classe MainStub dans la variable 
“algo” 
“R” équivaut au parcours aléatoire 
“P” équivaut à l’algorithme de Prim 
“A” équivaut à l’algorithme d'Aldous Broder 
“BFS” équivaut au Parcours en largeur existant dans le projet initial 


